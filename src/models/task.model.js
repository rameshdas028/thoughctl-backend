"use strict";
const mongoose = require('mongoose');

const taskSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
        type: String,
        default:""
    },
    DateAssigned: {
        type: Date,
        default:"",
    },
    DateCompleted: {
        type: Date,
        default:"",
    },
    DateClosed: {
        type: Date,
        default:"",
    },
    status: {
      type: String,
      enum: ['Draft','Assigned','In-Progress','Completed','Closed'],
      deafult:"Draft"
    },
    CreatedBy: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
        required: true,
    },
    AssignedTo: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
        default:null
    },

  },
  {
    timestamps: true,
  }
);
const Task = mongoose.model('Task', taskSchema);
module.exports = Task;
