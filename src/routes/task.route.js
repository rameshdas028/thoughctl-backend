'use strict';
const express = require('express');
const router = express.Router();
const validate = require('../middlewares/validate');
const taskValidation = require('../validations/task.validation');
const TaskController = require('../controllers/task.controller');
const auth = require('../middlewares/auth');
const access = require('../middlewares/access');

router.post('/', auth,access(['teamLead']),validate(taskValidation.create), TaskController.create);
router.get('/:id', auth,access(['teamLead']),validate(taskValidation.id), TaskController.view);
router.put('/:id', auth,access(['teamLead']),validate(taskValidation.update), TaskController.update);
router.delete('/:id', auth,access(['teamLead']),validate(taskValidation.id), TaskController.destroy);
router.get('/member/:id', auth,access(['teamLead','member']),validate(taskValidation.id), TaskController.getTaskByMemberId);
router.put('/:id/assign/:memberId', auth,access(['teamLead']),validate(taskValidation.assign), TaskController.assignTaskToMember);
router.put('/:id/accept', auth,access(['member']),validate(taskValidation.id), TaskController.acceptTask);
router.put('/:id/complete', auth,access(['member']),validate(taskValidation.id), TaskController.completedTask);
router.put('/:id/close', auth,access(['teamLead']),validate(taskValidation.id), TaskController.closeTask);


/**
 * @swagger
 * tags:
 *   name: Task
 *   description: task management and retrieval
*/

/**
 * @swagger
 * 
 */




module.exports = router;
