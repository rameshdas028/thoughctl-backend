"use strict";
const express = require('express');
const router = express.Router();

const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const taskRoute = require('./task.route');
const config = require('../config/config');


router.use('/auth', authRoute);
// router.use('/user', userRoute);
router.use('/task', taskRoute);
if (config.env === 'development') {
  router.use('/docs', docsRoute);
}
module.exports = router;
