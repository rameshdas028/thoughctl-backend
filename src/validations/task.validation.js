'use strict';
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);


exports.create = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().allow(''),
    DateAssigned: Joi.date().iso().greater('now').required(),
    DateCompleted: Joi.date().iso().greater(Joi.ref('DateAssigned')).allow(''),
    DateClosed: Joi.date().iso().greater(Joi.ref('DateAssigned')).required(),
    status: Joi.string().default('Draft'),
  }),
};
exports.id ={
  params:Joi.object().keys({
    id:Joi.objectId()
  })
}
exports.update = {
  params:Joi.object().keys({
    id:Joi.objectId()
  }),
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().allow(''),
    DateAssigned: Joi.date().iso().greater('now').required(),
    DateCompleted: Joi.date().iso().greater(Joi.ref('DateAssigned')).allow(''),
    DateClosed: Joi.date().iso().greater(Joi.ref('DateAssigned')).required(),
    status: Joi.string().default('Draft'),
  }),
};
exports.assign ={
  params:Joi.object().keys({
    id:Joi.objectId(),
    memberId:Joi.objectId(),
  })
}

