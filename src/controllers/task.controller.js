'use strcit';
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const taskService = require('../services/task.service');


exports.create = catchAsync(async (req, res) => {
    req.body.CreatedBy = req.user._id;
    let data = await taskService.create(req.body);
    let response = {
        statusCode: httpStatus.CREATED,
        message: "Task is successfully created.",
        data
    }
    return res.status(httpStatus.CREATED).send(response);
});
exports.view = catchAsync(async (req, res) => {
    let data = await taskService.view(req.params.id);
    let response = {
        statusCode: httpStatus.OK,
        message: ".",
        data
    }
    return res.status(httpStatus.OK).send(response);
});
exports.update = catchAsync(async (req, res) => {
    delete req.body.CreatedBy
    let data = await taskService.update(req.params.id, req.body);
    let response = {
        statusCode: httpStatus.OK,
        message: "Task is successfully updated.",
        data
    }
    return res.status(httpStatus.OK).send(response);
});
exports.destroy = catchAsync(async (req, res) => {
    let data = await taskService.destroy(req.params.id);
    let response = {
        statusCode: httpStatus.OK,
        message: "Task is successfully deleted.",
    }
    return res.status(httpStatus.OK).send(response);
});
exports.getTaskByMemberId = catchAsync(async (req, res) => {
    let data = await taskService.getTaskByMemberId(req.params.id);
    let response = {
        statusCode: httpStatus.OK,
        message: "",
        data
    }
    return res.status(httpStatus.OK).send(response);
});
exports.assignTaskToMember = catchAsync(async (req, res) => {
    const { id,memberId} =req.params;
    let data = await taskService.assignTaskToMember(id,memberId);
    let response = {
        statusCode: httpStatus.OK,
        message: "Task is successfully assign.",
        data
    }
    return res.status(httpStatus.OK).send(response);

});
exports.acceptTask = catchAsync(async (req, res) => {
    const { id} =req.params;
    let data = await taskService.acceptTask(id,req.user._id);
    let response = {
        statusCode: httpStatus.OK,
        message: "Task is successfully accepted.",
        data
    }
    return res.status(httpStatus.OK).send(response);

});
exports.completedTask = catchAsync(async (req, res) => {
    const { id} =req.params;
    let data = await taskService.completedTask(id,req.user._id);
    let response = {
        statusCode: httpStatus.OK,
        message: "Task is successfully completed.",
        data
    }
    return res.status(httpStatus.OK).send(response);

});
exports.closeTask = catchAsync(async (req, res) => {
    const { id} =req.params;
    let data = await taskService.closeTask(id,req.user._id);
    let response = {
        statusCode: httpStatus.OK,
        message: "Task is successfully closed.",
        data
    }
    return res.status(httpStatus.OK).send(response);

});

