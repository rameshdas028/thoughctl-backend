'use strict';
const httpStatus = require("http-status");
const { verify } = require("jsonwebtoken");
const User = require('../models/user.model');
const config = require('../config/config');

module.exports =  (req, res, next) => {
  try {
    const token = req.rawHeaders[1].split(" ")[1];
    if(typeof token =="undefined"){
      return res.status(httpStatus.BAD_REQUEST).json({
        status:httpStatus.BAD_REQUEST,
        message:"Please login !"
      })
    }
    const decodedToken = verify(token, config.jwt.secret,(error,authData)=>{
      if(error){
        return res.status(httpStatus.FORBIDDEN).json({
          status:httpStatus.FORBIDDEN,
          message:"Token has been expired!"
        })
      }else{
        User.findById(authData.sub).exec().then(data=>{
          req.user=data;
          next()
        }).catch(err=>{
          res.status(401).json({
            error: new Error("Invalid request!")
          });

        })
      }
    });
  } 
  catch {
    console.log(error);
    res.status(401).json({
      error: new Error("Invalid request!")
    });
  }
};
