'use strict';
const httpStatus = require("http-status");
const { verify } = require("jsonwebtoken");
const User = require('../models/user.model');
const config = require('../config/config');

module.exports =(roles)=> (req,res,next) => {
    try {
        if (roles.includes(req.user.role)) {
            next();
        } else {
            return res.status(httpStatus.BAD_REQUEST).json({
                status: httpStatus.BAD_REQUEST,
                message: "You have no access to granted."
            })
        }
    }
    catch {
        return res.status(httpStatus.BAD_REQUEST).json({
            status: httpStatus.BAD_REQUEST,
            message: "You have no access to granted."
        })
    }
};
