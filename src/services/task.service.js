'use strict';
const httpStatus = require('http-status');
const Task = require('../models/task.model');
const ApiError = require('../utils/ApiError');

exports.create= async (reqBody) =>{
    let data = await Task.create(reqBody);
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task not created.');
    return data;
}
exports.view= async (id) =>{
    let data = await Task.findById(id);
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
}
exports.update= async (id,reqBody) =>{
    let  data =  await Task.findOneAndUpdate({ _id:id,status:{$ne:'Closed'}},reqBody,{new:true})
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
};
exports.destroy= async (id) =>{
    let  data =  await Task.findOneAndDelete({ _id:id,status:{$ne:'Closed'}})
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
};
exports.getTaskByMemberId= async (id) =>{
    let data = await Task.find({AssignedTo:id});
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
}
exports.assignTaskToMember= async (id,memberId) =>{
    let reqBody={
        "AssignedTo":memberId,
        "status":"Assigned"
    };
    let data = await Task.findOneAndUpdate({ _id:id,status:{$ne:'Closed'}},reqBody,{new:true});
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
};
exports.acceptTask= async (id,memberId) =>{
    let reqBody={
        "status":"In-Progress",
    };
    let findQuery={ 
        _id:id,
        AssignedTo:memberId,
        status:{$ne:'Closed'}
    }
    let data = await Task.findOneAndUpdate(findQuery,reqBody,{new:true});
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
}
exports.completedTask= async (id,memberId) =>{
    let reqBody={
        "status":"Completed",
        "DateCompleted":new Date()
    };
    let findQuery={ 
        _id:id,
        AssignedTo:memberId,
        status:{$ne:'Closed'}
    }
    let data = await Task.findOneAndUpdate(findQuery,reqBody,{new:true});
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
}

exports.closeTask= async (id,teamLeadId) =>{
    let reqBody={
        "status":"Closed",
    };
    let findQuery={ 
        _id:id,
        CreatedBy:teamLeadId,
        status:{$ne:'Closed'}
    }
    let data = await Task.findOneAndUpdate(findQuery,reqBody,{new:true});
    if(!data) throw new ApiError(httpStatus.BAD_REQUEST,'Task does not found.');
    return data;
}

